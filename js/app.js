var $users = [
    {
        id: 1,
        name: "Jebediah",
        address: "123 Main Street",
        age: 19,
    },
    {
        id: 2,
        name: "Jameson",
        address: "4967 Poplar Level",
        age: 23,
    },
];

$.each($users, function(i, user){
       appendToUsrTable(user);
       
});


// Prevent form from subbmitting
$('form').submit(function(e){ // targets the from element
                 e.preventDefault();
                 }); 


//Add User to Users Table

function addUser(user){
    appendToUsrTable(user);
    
}
    // 1.Get data from Form
$('form#addUser').submit(function(){
    var $user = {};
    var $nameInput = $('input[name="name"]').val().trim();
    var $addressInput = $('input[name="address"]').val().trim();
    var $ageInput = $('input[name="age"]').val().trim();;
    
    if($nameInput && $addressInput && $ageInput){
        $(this).serializeArray().map(function(data){
            $user[data.name] = data.value;          
        });
        
        var $lastUser = $users[Object.keys($users).sort().pop()];
    $users.id = $lastUser.id + 1;
    
    addUser($user);
}else{ 
        alert("All fields must have a valid value.")        
}
    $(this).closest('form').find("input[name]").val("")
    
});
    // 2.Display data s


function appendToUsrTable(user){
    $('#userTable > tbody:last-child').append(`
        <tr>
            <td class="userData" name="name">${user.name}</td>
            <td class="userData" name="address">${user.address}</td>
            <td class="userData" name="age">${user.age}</td>
        </tr>
    `);
}





















